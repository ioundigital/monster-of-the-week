@extends('templates.user')

@section('panel')
    <div class="container">
            @if( count( $hunters ) )
            <div class="row">
                <div class="col-sm-12">
                    <h2>Hunters</h2>
                </div>
            </div>
            <div class="row">
                @foreach( $hunters as $hunter )
                    <div class="col-md-3">
                        
                    </div>
                @endforeach
                <!-- list created hunters here -->
            </div>
            @endif
            @if( count( $mysteries ) )
            <div class="row">
                <div class="col-sm-12">
                    <h2>Mysteries</h2>
                </div>
            </div>
            <div class="row">
                <!-- list active keeper mysteries here -->
            </div>
            @endif
            
            @if( count( $mysteries) <= 0 && count( $hunters ) <= 0 )
                <div class="row justify-content-center">
                    <div class="col-sm-6 ">
                        <h2>Woah!</h2>
                        <p>Doesn't look like there's anything here yet. Why not start a new campaign? Or if you're here to join a mystery, get started there!</p>
                    </div>
                </div>
            @endif
    </div>
@endsection

@section('scripts')
@endsection