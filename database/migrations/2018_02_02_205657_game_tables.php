<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GameTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'mysteries', function( Blueprint $t ) {
            
            $t->increments('id');
            $t->integer('keeper_id');
            $t->string('session_token');
            $t->longText('hook');
            $t->longText('day');
            $t->longText('shadows');
            $t->longText('dusk');
            $t->longText('sunset');
            $t->longText('nightfall');
            $t->longText('midnight');
            $t->string('stage');
            $t->longText('notes');
            
            $t->timestamps();
            $t->softDeletes();
            
        });
        
        Schema::create( 'monster_mystery', function( Blueprint $t ) {
            
            $t->increments('id');
            $t->integer('monster_id');
            $t->integer('mystery_id');
            
        });
        
        Schema::create( 'bystander_mystery', function( Blueprint $t ) {
            
            $t->increments('id');
            $t->integer('bystander_id');
            $t->integer('mystery_id');
            
        });
        
        Schema::create( 'minion_mystery', function( Blueprint $t ) {
            
            $t->increments('id');
            $t->integer('minion_id');
            $t->integer('mystery_id');
            
        });
        
        Schema::create( 'hunter_mystery', function( Blueprint $t ) {
            
            $t->increments('id');
            $t->integer('hunter_id');
            $t->integer('mystery_id');
            
        });
        
        Schema::create( 'monsters', function( Blueprint $t ) {
            
            $t->increments('id');
            $t->string('name');
            $t->integer('type_id');
            $t->longText('description');
            $t->longText('powers');
            $t->longText('attacks');
            $t->integer('harm_capacity');
            $t->integer('harm_taken');
            $t->integer('base_armor');
            $t->longText('weakness');
            $t->longText('motive');
            
            $t->timestamps();
            $t->softDeletes();
            
        });
        
        Schema::create( 'item_monster', function( Blueprint $t ) {
            
            $t->increments('id');
            $t->integer('item_id');
            $t->integer('monster_id');
            
        });
        
        
        Schema::create( 'minions', function( Blueprint $t ) {
            
            $t->increments('id');
            $t->string('name');
            $t->integer('type_id');
            $t->longText('description');
            $t->longText('powers');
            $t->longText('attacks');
            $t->integer('harm_capacity');
            $t->integer('harm_taken');
            $t->integer('base_armor');
            $t->longText('motive');
            $t->longText('notes');
            
            $t->timestamps();
            $t->softDeletes();
            
        });
        
        Schema::create( 'item_minion', function( Blueprint $t ) {
            
            $t->increments('id');
            $t->integer('item_id');
            $t->integer('minion_id');
            
        });
        
        Schema::create( 'bystanders', function( Blueprint $t ) {
            
            $t->increments('id');
            $t->string('name');
            $t->integer('type_id');
            $t->integer('harm_capacity');
            $t->integer('harm_taken');
            $t->longText('description');
            $t->longText('motive');
            $t->longText('notes');
            
            $t->timestamps();
            $t->softDeletes();
            
        });
        
        Schema::create( 'bystander_item', function( Blueprint $t ) {
            
            $t->increments('id');
            $t->integer('bystander_id');
            $t->integer('item_id');
            
        });
        
        Schema::create( 'hunters', function( Blueprint $t ) {
            
            $t->increments( 'id' );
            $t->integer('user_id');
            $t->string('name');
            $t->integer('archetype_id');
            $t->integer('charm');
            $t->integer('cool');
            $t->integer('sharp');
            $t->integer('tough');
            $t->integer('weird');
            $t->integer('luck_capacity');
            $t->integer('luck_used');
            $t->integer('harm_capacity');
            $t->integer('harm_taken');
            $t->boolean('unstable');
            $t->integer('experience');
            $t->integer('level');
            $t->longText('moves');
            $t->longText('archetype_data');
            $t->longText('player_notes');
            $t->longText('keeper_notes');
            
            $t->timestamps();
            $t->softDeletes();
        });
        
        Schema::create( 'archetypes', function( Blueprint $t ) {
            
            $t->increments('id');
            $t->string('name');
            $t->longText('moves');
            $t->longText('ratings');
            $t->longText('improvements');
            $t->longText('archetype_data');
            
            $t->timestamps();
            $t->softDeletes();
            
        });
        
        Schema::create( 'types', function( Blueprint $t ) {
            
            $t->increments('id');
            $t->string('name');
            $t->longText('generice_motive');
            $t->string('attributes_to');
            
            $t->timestamps();
            $t->softDeletes();
            
        });
        
        Schema::create('items', function( Blueprint $t ) {
            
            $t->increments('id');
            $t->string('name');
            $t->integer('itemtype_id');
            $t->integer('harm');
            $t->string('range');
            $t->longText('tags');
            $t->longText('notes');
            
            $t->timestamps();
            $t->softDeletes();
            
        });
        
        Schema::create('item_templates', function( Blueprint $t ) {
            
            $t->increments('id');
            $t->string('name');
            $t->integer('itemtype_id');
            $t->integer('harm');
            $t->string('range');
            $t->longText('tags');
            
            $t->timestamps();
            $t->softDeletes();
            
        });
        
        Schema::create('itemtypes', function( Blueprint $t ) {
            
            $t->increments('id');
            $t->string('name');
            $t->longText('dscription');
            
            $t->timestamps();
            $t->softDeletes();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('itemtypes');
        Schema::drop('items');
        Schema::drop('item_templates');
        Schema::drop('types');
        Schema::drop('archetypes');
        Schema::drop('hunters');
        Schema::drop('bystander_item');
        Schema::drop('bystander');
        Schema::drop('item_minion');
        Schema::drop('minions');
        Schema::drop('item_monster');
        Schema::drop('monsters');
        Schema::drop('hunter_mystery');
        Schema::drop('minion_mystery');
        Schema::drop('bystander_mystery');
        Schema::drop('monster_mystery');
        Schema::drop('mysteries');
    }
}
