<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;
    
    public function monster()
    {
        return $this->belongsTo('App\Monster');
    }
    
    public function minion()
    {
        return $this->belongsTo('App\Minion');
    }
    
    public function bystander()
    {
        return $this->belongsTo('App\Bystander');
    }
    
    public function itemtype()
    {
        return $this->belongsTo('App\Itemtype');
    }
}