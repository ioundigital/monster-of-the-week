<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hunter extends Model
{
    use SoftDeletes;
    
    public function mysteries()
    {
        return $this->belongsToMany('App\Mystery');
    }
    
    public function archetype()
    {
        return $this->belongsTo('App\Archetype');
    }
}