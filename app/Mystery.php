<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mystery extends Model
{
    use SoftDeletes;
    
    public function monsters()
    {
        return $this->hasMany('App\Monster');
    }
    
    public function minions()
    {
        return $this->hasMany('App\Minion');
    }
    
    public function bystanders()
    {
        return $this->hasMany('App\Bystander');
    }
    
    public function hunters()
    {
        return $this->hasMany('App\Hunter');
    }
}