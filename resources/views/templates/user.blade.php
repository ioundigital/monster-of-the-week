@extends('templates.master')

@section('head')
    <link rel="stylesheet" href="{{ asset('css/user.css') }}"/>
@endsection

@section('content')
    @yield('panel')
@endsection