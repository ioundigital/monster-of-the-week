<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    
    protected function render( $view, $data = [] )
    {
        if( !isset( $data['title'] ) ) {
            $data['title'] = 'MOTW Online';
        } else {
            $data['title'] = $data['title'] . ' | MOTW Online';
        }
        return view( $view, $data );
    }
}
