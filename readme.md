# Monster Of The Week - Online Tool

A browser based tool designed and built to be used with the [Monster Of The Week](https://www.evilhat.com/home/monster-of-the-week/) game by Evil Hat.

Basically this just keeps track of all the details and makes it visible online for your Keeper. In the future, maybe more?

## Change Log

###0.1.0
- Create game database structure migration
- Build landing pages
- Theme auth pages appropriately
- Create framework for user home page

###0.2.0
- Build out user home page


## Roadmap

###0.1.0

- Get the basic database structure built and in place
- Build out landing pages

### 0.2.0

- Build out user registration system
- Build user home page

### 0.3.0

- Build keeper mystery setup form
- Build session view framework
    - Common header bar
    - Keeper information display placeholders
    - Hunter information display placeholders
    - Quick info dialogues

### 0.4.0

- Build the hunter cards
- Create archetypes for core hunter types
- Build monster, minion, and bystander cards
- Replace framework placeholders with appropriate cards

### 0.5.0

- Add inventory management
- Build the "First Session" forms
    - Hunter cards
    - Archetype selection form
- Get socket.io working with archetype selection and player ready states

### 0.6.0

- Get socket.io working with monster, minion, hero, and bystander cards
- Get socket.io working with session view header bar
- Add session locking functionality for Keeper

### 0.7.0

- Build level up dialogues for heros
- Build notification system

### 1.0.0

- Finalize styling
- Setup URL and Launch