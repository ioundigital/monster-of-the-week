<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Archetype extends Model
{
    use SoftDeletes;
    
    public function mysteries()
    {
        return $this->belongsToMany('App\Mystery');
    }
}