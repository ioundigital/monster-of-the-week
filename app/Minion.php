<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Minion extends Model
{
    use SoftDeletes;
    
    public function mysteries()
    {
        return $this->belongsToMany('App\Mystery');
    }
    
    public function type()
    {
        return $this->belongsTo('App\Type');
    }
    
    public function items()
    {
        return $this->hasMany('App\Item');
    }
}