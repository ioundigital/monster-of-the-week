<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Monster;
use App\Mystery;

class HomeController extends Controller
{
    private $user;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->user = Auth::user();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->user = Auth::user();
        $data['hunters'] = $this->user->hunters;
        $data['mysteries'] = $this->user->mysteries;
        return view('home', $data);
    }
}
