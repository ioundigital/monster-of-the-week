@extends('templates.master')

@section('content')
    <div id="homeSlider" class="carousel slide" style="max-height:550px; overflow:hidden">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img alt="Mountain Man artwork" src="{{ asset('img/train.jpg') }}" class="img-fluid" />
            </div>
            <div class="carousel-item">
                <img alt="Tunnel Future artwork" src="{{ asset('img/tunnel.jpg') }}" class="img-fluid" />
            </div>
            <div class="carousel-item">
                <img alt="Tunnel Future artwork" src="{{ asset('img/forest.jpg') }}" class="img-fluid" />
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Welcome, Hunters and Keepers</h1>
                <p class="lead">MOTW Online is a digital record keeper for your <a href="https://www.evilhat.com/home/monster-of-the-week/" target="_blank">Monster Of The Week</a> games! We provide a browser-based environment for both your Hunters and Keeper, that displays everything in-sync, no matter where your players are. To get started, have your Keeper sign up and get started with our easy to use Mystery Runner.</p>
                <h2>Evil Hat</h2>
                <p class="lead">We want to offer a special thanks to Evil Hat and all involved in creating the game this tool is based on, for being generous with the rights to use their system, and for providing endless hours of high quality adventures.</p>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            $('.carousel').carousel({
                interval: 8000,
                pause: false,
            });
        })
    </script>
@endsection